echo off
rem Delete bin folder
rmdir /s /q .\bin
rem ---------------------------------------------------------------------------
echo Creating folder bin...
mkdir bin 2>nul
rem ---------------------------------------------------------------------------
echo Compiling...
javac -sourcepath ./src/main/java -d bin src/main/java/com/gmail/smaglenko/anagrams/Main.java
if %ERRORLEVEL% NEQ 0 (
  echo "Compile error..."
  rd /s /q ..\bin  
  exit /b
)
rem ---------------------------------------------------------------------------
echo ...successful 
rem ---------------------------------------------------------------------------
echo Packaging a jar...
pushd .\bin\
echo Main-Class: com.gmail.smaglenko.anagrams.Main> "Manifest.txt"
jar cfm anagrams.jar Manifest.txt com/gmail/smaglenko/anagrams/Anagrams.class com/gmail/smaglenko/anagrams/Main.class
move /y ../bin/anagrams.jar ../dist
rem ---------------------------------------------------------------------------
popd
rem ---------------------------------------------------------------------------
echo ...successful 