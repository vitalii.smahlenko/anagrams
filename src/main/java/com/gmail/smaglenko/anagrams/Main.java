package com.gmail.smaglenko.anagrams;

public class Main {
    public static void main(String[] args) {
        Anagrams anagrams = new Anagrams();
        for (int i = 0; i < args.length; i++) {
            System.out.print(args[i] + " => ");
            System.out.println(anagrams.reverseText(args[i]));
        }
    }
}