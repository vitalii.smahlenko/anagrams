package com.gmail.smaglenko.anagrams;

/**
 * Application Anagrams should reverse all the words of input text:
 * E.g. "abcd efgh" => "dcba hgfe"
 * All non-letter symbols should stay on the same places:
 * E.g. "a1bcd efg!h" => "d1cba hgf!e"
 * Use Latin alphabet for test only.
 */
public class Anagrams {
    static final String ONE_AND_MORE_WHITESPACES_REGEXP = "\\s+";
    static final String ONE_AND_MORE_SPACES_REGEXP = "\\S+";

    public String reverseWord(String word) {
        char[] wordSymbols = word.toCharArray();
        for (int i = 0, j = wordSymbols.length - 1; i < j; ) {
            if (!Character.isLetter(wordSymbols[i])) {
                i++;
            } else if (!Character.isLetter(wordSymbols[j])) {
                j--;
            } else {
                char temp = wordSymbols[i];
                wordSymbols[i] = wordSymbols[j];
                wordSymbols[j] = temp;
                i++;
                j--;
            }
        }
        return String.valueOf(wordSymbols);
    }

    public String reverseText(String text) {
        String[] words = text.split(ONE_AND_MORE_WHITESPACES_REGEXP);
        String[] spaces = text.split(ONE_AND_MORE_SPACES_REGEXP);
        for (int i = 0; i < words.length; i++) {
            words[i] = reverseWord(words[i]);
        }
        StringBuilder sb = new StringBuilder();
        String inputWord = null;
        for (int i = 0; i < words.length; i++) {
            sb.append(spaces[i]).append(words[i]);
            inputWord = sb.substring(0, sb.length());
        }
        return inputWord;
    }
}