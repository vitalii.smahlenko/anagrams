package com.gmail.smaglenko.anagrams;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AnagramsTest {

    Anagrams anagrams;

    @BeforeEach
    public void sepUp() {
        anagrams = new Anagrams();
    }

    @Test
    public void reverseWord_shouldRunWell_whenInputEmptyString() {

        String result = anagrams.reverseWord("");

        assertEquals("", result, "The method should run well");
    }

    @Test
    public void reverseWord_shouldReverseAllCharacters_whenInputLatinLettersOnly() {

        String result = anagrams.reverseWord("abcdEFGH");

        assertEquals("HGFEdcba", result, "The method should reverse all letters");
    }

    @Test
    public void characters_shouldStaySamePlaces_whenInputNumbersOnly() {

        String result = anagrams.reverseWord("123456");

        assertEquals("123456", result, "All numbers should stay in the same " +
                "places");
    }

    @Test
    public void characters_shouldStaySamePlaces_whenInputSpecialCharactersOnly() {

        String result = anagrams.reverseWord("@#$%<>");

        assertEquals("@#$%<>", result,
                "All special should symbols stay in the same places");
    }

    @Test
    public void nonLetterCharacters_shouldStaySamePlace_whenInputLettersAndNumbers() {

        String result = anagrams.reverseWord("1a2b3C4D");

        assertEquals("1D2C3b4a", result, "All non-letter symbols should stay on " +
                "the same places");
    }

    @Test
    public void specialCharacters_shouldStaySamePlace_whenInputLettersAndSpecialCharacters() {

        String result = anagrams.reverseWord("!a@b#C$D");

        assertEquals("!D@C#b$a", result, "All special symbols should stay on" +
                " the same places");
    }

    @Test
    public void characters_shouldStaySamePlaces_whenInputNumbersAndSpecialCharacters() {

        String result = anagrams.reverseWord("1234!@#$");

        assertEquals("1234!@#$", result, "All characters should stay on " +
                "the same places");
    }

    @Test
    public void nonLetterCharacters_shouldStaySamePlace_whenInputLettersNumbersAndSpecialCharacters() {

        String result = anagrams.reverseWord("2JxU!GkP@");

        assertEquals("2PkG!UxJ@", result, "All non-letter symbols should stay on" +
                " the same places");
    }

    @Test
    public void reverseWord_shouldThrowsNullPointerException_whenInputNull() {
        assertThrows(NullPointerException.class, new Executable() {
                    public void execute() throws Throwable {
                        anagrams.reverseWord(null);
                    }
                },
                "ReverseWord should throws NullPointerException if input " +
                        "Null");
    }

    @Test
    //    Separator between String is Tab
    public void reverseText_shouldRunWell_whenSpaceTab() {

        String expect = "2GkP!   JxU@";
        String result = anagrams.reverseText("2PkG!   UxJ@");

        assertEquals(expect, result, "reverseText should run well if " +
                "the space between the words is Tab");


    }

    @Test
    //    Separator between String is \r
    public void reverseText_shouldRunWell_whenSpaceRegexCarriageReturnCharacter() {

        String expect = "2GkP!\rJxU@";
        String result = anagrams.reverseText("2PkG!\rUxJ@");

        assertEquals(expect, result, "reverseText should run well if " +
                "the space between the words is" +
                " \r carriage-return character");

    }

    @Test
    //    Separator between String is \n
    public void reverseText_shouldRunWell_whenSpaceRegexNewline() {

        String expect = "2GkP!\nJxU@";
        String result = anagrams.reverseText("2PkG!\nUxJ@");

        assertEquals(expect, result, "reverseText should run well if " +
                "the space between the words is " +
                "\n newline (line feed) character");

    }

    @Test
    //    Separator between String is \t
    public void reverseText_shouldRunWell_whenSpaceRegexTabCharacter() {

        String expect = "2GkP!\tJxU@";
        String result = anagrams.reverseText("2PkG!\tUxJ@");

        assertEquals(expect, result, "reverseText should run well if " +
                "the space between the words is" +
                " \t tab character");
    }

    @Test
    //    Separator between String two spase
    public void reverseText_shouldRunWell_whenTwoSpace() {

        String expect = "2GkP!  JxU@";
        String result = anagrams.reverseText("2PkG!  UxJ@");

        assertEquals(expect, result, "reverseText should run well if " +
                "the space between the words is" +
                " two space");
    }
}

