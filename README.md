**Task 1 - Anagrams**

Write an application that reverses all the words of input text:

  E.g. "abcd efgh" => "dcba hgfe"

All non-letter symbols should stay on the same places:

  E.g. "a1bcd efg!h" => "d1cba hgf!e"

Use Latin alphabet for test only.

![run-1](/uploads/f6dcd907f97c57b5195d92b08c54724b/run-1.jpg)

![run-2](/uploads/4f2ca6ec2ec211e0326b5cf52e27e1b6/run-2.jpg)

![run-3](/uploads/1fcb82d1b8b70982ea594a4370d1c551/run-3.jpg)

**Task 2 - Unit tests**

Write JUnit tests for previous application.

![MainTest](/uploads/59ddb5301b45ae6515cf9a4b23e07a47/MainTest.jpg)

![AnagramsTest](/uploads/d968611fd46e580c7e1902762d516997/AnagramsTest.jpg)

**Task 3 - Maven and other useful tools**

Convert your previous project to a maven project. 

Add Sonar lint plugin support to your IDE.

Add Editorconfig support to your project.

![anagrams-1.0-SNAPSHOT-1](/uploads/42fab088a96e90f8e117051d96f2c0ff/anagrams-1.0-SNAPSHOT-1.jpg)

![anagrams-1.0-SNAPSHOT-3](/uploads/410674cb3251c4f15b20fc34e85a48f2/anagrams-1.0-SNAPSHOT-3.jpg)

![anagrams-1.0-SNAPSHOT-2](/uploads/520e815ae8d8b128c1adc3e8b4236bf1/anagrams-1.0-SNAPSHOT-2.jpg)

