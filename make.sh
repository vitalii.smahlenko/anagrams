echo off
#-----------------------Delete bin folder-------------------------------------------
rm -rf ./bin 
#-----------------------------------------------------------------------------------
echo Creating folder bin... 
mkdir bin 2>nul
#-----------------------------------------------------------------------------------
echo Compiling...
javac -sourcepath ./src/main/java -d bin ./src/main/java/com/gmail/smaglenko/anagrams/Main.java
if [ $? -eq 0 ]
    then
        echo "...successful"
    else
        echo "Compile error... " >&2
        exit 1
fi 
#-----------------------------------------------------------------------------------
echo Packaging a jar...
pushd ./bin
echo Main-Class: com.gmail.smaglenko.anagrams.Main> "Manifest.txt"
jar cfm anagrams.jar Manifest.txt ./com/gmail/smaglenko/anagrams/Anagrams.class ./com/gmail/smaglenko/anagrams/Main.class
#-----------------------Move anagrams.jar to dist folder----------------------------
mv anagrams.jar ../dist 
#-----------------------------------------------------------------------------------
popd
#-----------------------------------------------------------------------------------
echo ...successful 