echo off
#---------------------------------------------------------
pushd ./dist
for x in "$@"; do
	java -jar anagrams.jar $x 
	echo '\r'
done
#---------------------------------------------------------
popd