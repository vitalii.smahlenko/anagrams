FROM openjdk:11-jre-slim
EXPOSE 8080
COPY /target/anagrams-1.0-SNAPSHOT.jar anagrams-1.0-SNAPSHOT.jar
CMD ["java","-jar","anagrams-1.0-SNAPSHOT.jar", "second       1ine", "a1bcd\tefg!h", "He11o W0r1d"]